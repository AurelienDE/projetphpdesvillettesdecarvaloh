<?php

try{
	$file_db = new PDO("sqlite:contacts.db");
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	

include('../Model/Connexionmodel.php');	
	
include('../Model/fichefilmmodel.php');

	
include('../Vue/Entete.php');	
?>
<body>	
<?php

session_start();

$mapage=basename(__FILE__);

/*
	Importe le header de la page web.
*/
include("../Vue/header.php"); 
 
/*
	Importe le menu de navigation de la page web.
*/
include("../Vue/menu.php");

?>

<?php 


?>	
	<section>
		<article>
	
	<?php	
	/*
		Ce code php permet de se connecter et de reload la page sur laquelle nous étions en gardant la connexion.
	*/
	//ob_start();
	if (!isset($_SESSION['login'])){ 
		include('../Vue/Seconnecter/Seconnectervue.php');	
	
	
		$mavar='';
		
		if(isset($_POST['login'])){
		
			$motdepasses=get_motdepasse($_POST['login']);
			
			foreach ($motdepasses as $idd){
				$mavar=$idd['motdepasse'];
				
			}
			
			$grade=get_grade($_POST['login']);
			foreach ($grade as $iddd){
				$desbar=$iddd['grade'];
				
			}
			
			
			
				
				if ((crypt_mdp($_POST['pass'])==$mavar)){
					$_SESSION['grade']=$desbar;
					$_SESSION['login']=$_POST['login'];
					
					if ($mapage=='filmz.php'){
						
						header("Location: ".$mapage."?z=".$monz."");
						
						
					}
					else{
						header("Location: ".$mapage."");
					}
				}
				else{
				
					$supervar="dd";
					include('../Vue/Connexionrefuseevue.php');
					
					header("Location: ".$mapage."?supervar='dd'");
					
				}
		}
		

	}
	else{
		 
		
		
			echo "<article>";
				echo "<form action='".$mapage."' method='POST'>";
					echo "<p>";
					echo "Bonjour ".$_SESSION['login'];
					echo "</p>";
					
				
					echo "<div id='deconnexion'><input type='submit' name='sedeco' value='Deconnexion'></div> ";
					echo "<br>";
					if(!empty($_POST['sedeco'])){
						session_destroy(); 
						header("Location: ".$mapage);
					}
					
					echo "</form>";
			echo "</article>";
		
	} 

	//ob_end_clean();
	?>	
</section>
	
</body>
 <?php
 }
catch(PDOException $e){
	$z=$e->getMessage();

	echo "<br>";
} ?>

<?php include("../Vue/footer.php"); ?>
</html>
