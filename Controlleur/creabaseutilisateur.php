<?php

include('../Model/Connexionmodel.php');

try{
	$file_db = new PDO("sqlite:contacts.db");
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
 	
 	
 	 //~ $file_db->exec("DROP TABLE utilisateur");
  
	
	 $file_db->exec("CREATE TABLE IF NOT EXISTS utilisateur (
	 				nom TEXT,
	 				prenom TEXT,
	 				email TEXT,
	 				login TEXT PRIMARY KEY,
	 				motdepasse TEXT,
	 				grade TEXT,
	 				nombremessage int)");
					 
	 
	 
	 $contacts= array(
	 				array("nom"=> "Desvillettes","prenom" =>"Aurelien", "email"=>"a.desvillettes@free.fr", "login"=>"Amayro", "motdepasse"=>crypt_mdp("admin"),"grade"=>"admin","nombremessage"=>0 ),
	 				array("nom"=> "Decarvalho","prenom" =>"William", "email"=>"w.decarvaloh@free.fr", "login"=>"Hunter45", "motdepasse"=>crypt_mdp("admin"),"grade"=>"admin","nombremessage"=>0 ),
	 				array("nom"=> "Fallot","prenom" =>"Adrien", "email"=>"f.fallot@free.fr", "login"=>"pixelcuicui", "motdepasse"=>crypt_mdp("admin"),"grade"=>"inscrit","nombremessage"=>0 ),
					array("nom"=> "Jeandel","prenom" =>"Marion", "email"=>"m.jeandel@free.fr", "login"=>"Starfoullah", "motdepasse"=>crypt_mdp("admin"),"grade"=>"inscrit","nombremessage"=>0 ),
	 				array("nom"=> "Deudon","prenom" =>"Jean", "email"=>"j.deudon@free.fr", "login"=>"redsteal", "motdepasse"=>crypt_mdp("admin"),"grade"=>"inscrit","nombremessage"=>0 ),
	 				array("nom"=> "Duval","prenom" =>"romain", "email"=>"r.duval@free.fr", "login"=>"Adralis", "motdepasse"=>crypt_mdp("admin"),"grade"=>"inscrit","nombremessage"=>0 ),
	 				array("nom"=> "Gann","prenom" =>"Remi", "email"=>"r.gann@free.fr", "login"=>"pancake", "motdepasse"=>crypt_mdp("admin"),"grade"=>"inscrit","nombremessage"=>0 ),
	 				);
	 
	 $insert= "INSERT INTO utilisateur (nom,prenom,email,login,motdepasse,grade,nombremessage) VALUES (:n, :p, :e, :l, :m, :g, :o)";
	 $stmt=$file_db->prepare($insert);
	 
	 $le_nom = null;
	 $le_prenom=null;
	 $le_email=null;
	 $le_login=null;
	 $le_motdepasse=null;
	 $le_grade=null;
	 $les_nombremessage=null;
	 
	 $stmt->bindParam(":n", $le_nom);
     $stmt->bindParam(":p", $le_prenom);
	 $stmt->bindParam(":e", $le_email);
	 $stmt->bindParam(":l", $le_login);
	 $stmt->bindParam(":m", $le_motdepasse);
	 $stmt->bindParam(":g", $le_grade);
	 $stmt->bindParam(":o", $les_nombremessage);
	 
	 foreach ($contacts as $c){
	 	$le_nom = $c["nom"];
	 	$le_prenom=$c["prenom"];
	 	$le_email=$c["email"];
	 	$le_login=$c["login"];
	     $le_motdepasse=$c["motdepasse"];
	 	$le_grade=$c["grade"];
	    $les_nombremessage=$c["nombremessage"];
				
     	$stmt ->execute();
	  }  
	  
	  echo "contacts en base";
	
}
catch(PDOException $e){
	echo $e->getMessage();
}

?>

