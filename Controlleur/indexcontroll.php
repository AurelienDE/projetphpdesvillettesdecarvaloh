<?php
try{
	$file_db = new PDO("sqlite:contacts.db");
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	

	include('../Model/indexmodel.php');
	
	session_start();
	$mapage=basename(__FILE__);
	
	include('../Vue/Entete.php');	

	/*
		*Affiche le header de la page.
	*/
	include("../Vue/header.php"); 
 
 	/*
		*Affiche le menu de navigation de la page.
 	*/
	include("../Vue/menu.php");

	/*
		*Affiche l'aside connexion.
	*/
	include("Connexion.php"); 
?>

<section>
	<article>
		<p>Bonjour sur ce site qui regroupe une base de donnée modifiable de films !<br><br> Inscrivez vous des maintenant pour pouvoir executez ces modifications</p>
		<br>
	</article>
	<article>
		<h2>Top film :</h2>
		<hr>
			<?php 
			/*
				* Programme pour afficher le film ayant le plus de commentaires.
			*/

			$lescoms=get_commentaire();
			$max=0;
			foreach ($lescoms as $x) {	
				$cpt_int=0;
				$com=explode("&|&",$x['commentaire']);
				for ($i=0; $i <count($com) ; $i++) {
					if(strlen($com[$i])!=0){
						$separ=explode("--|--",$com[$i]);
						for ($j=0; $j <count($separ) ; $j++) { 
							if($j%2==1){
								$cpt_int=$cpt_int+1;
								if ($cpt_int>$max){
									$max=$cpt_int;
								}				
							}
						}
					}	
				}			
			}
			$mescometmesnoms=get_commentaireetnom();
			foreach ($mescometmesnoms as $aa) {	
				$cpt_int=0;
				$com=explode("&|&",$aa['commentaire']);
				for ($i=0; $i <count($com) ; $i++) { 
					if(strlen($com[$i])!=0){
						$separ=explode("--|--",$com[$i]);
						for ($j=0; $j <count($separ) ; $j++) { 
							if($j%2==1){
								$cpt_int=$cpt_int+1;
								if ($cpt_int==$max){
									$desvideosenfonctiondunnom=get_videoenfonctiondunnom($aa['nom']);
									echo "<table id='onefiche'>";
										foreach ($desvideosenfonctiondunnom as $x) {
											echo "<tr>";
												echo "<th colspan='4' >".$x['nom']."</th>";
											echo "</tr>";

											echo "<tr>";
												echo "<td colspan='2'><img class=".'grossefiche'." src='".$x['image']."'/></td>";
												echo "<td colspan='2' class='supercellulle' style='width:500px';> <b>Resumé : </b>".$x['presentation']."</td>";
											echo "</tr>";

											echo "<tr>";
												echo "<td colspan='2'><b>Realisateur :</b> ".$x['realisateur']."</td>";
												echo "<td colspan='2'><b>Année :</b> ".$x['annee']."</td>";
											echo "</tr>";

											echo "<tr>";
												echo "<td colspan='2'><b>Genre :</b> ".$x['genre']."</td>";
												echo "<td colspan='2'><b>Acteurs :</b> ".$x['acteur']."</td>";
											echo "</tr>";	
										}
									echo "</table>";
								}							
							}
						}
					}
				}
			}
		?>
	
		
 <?php
 }
catch(PDOException $e){
	$z=$e->getMessage();
	echo "<br>";
} ?>
		
	</article>
</section>		
</body>
	<?php include("../Vue/footer.php"); ?>
</html>
