
<?php

try{
	$file_db = new PDO("sqlite:contacts.db");
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	

include('../Model/Filmmodel.php');
	
include('../Model/Membremodel.php');

session_start();

$mapage=basename(__FILE__);
	
	
	
include('../Vue/Entete.php');	

/*
	Affiche le header de la page.	
*/	
include("../Vue/header.php"); 

/*
	Affiche le menu de navigation de la page.	
*/	
include("../Vue/menu.php");

/*
	Affiche l'aside connexion de la page.	
*/	
include("Connexion.php"); 
	
 
	ob_start();
	$utilisateurs=get_utilisateurs();
	/*
		Affiche tout les utilisateurs par fiche.
	*/
	include('../Vue/Membres/touslesutilisateursvue.php');
	echo "<br>";
	if(isset($_SESSION['login'])){
		if ($_SESSION['grade']=='admin'){
			/*
				Creer une liste à partir des noms de tout les membres avec les boutons supprimer, upgrade et ungrade.
			*/
			echo "<form method=\"post\">";
			echo "<p><select name=\"menumembre\">";
			$i=0;
			$result= $file_db->query('Select * FROM utilisateur');
			foreach ($result as $s){
				echo "<option value=".$s['prenom'].">".$s['prenom']."</option>";
				$i++;
			}
			echo "<input type=\"submit\" name=\"supprimer\" value=\"Supprimer\"/>";
			echo "<input type=\"submit\" name=\"ungrade\" value=\"Downgrade\"/>";
			echo "<input type=\"submit\" name=\"upgrade\" value=\"Upgrade\"/></form>";
		}
		if(isset($_POST['supprimer'])){

			del_utilisateurs($_POST["menumembre"]);
			ob_end_clean();
			header("Location: membrecontrol.php");
		}
		if(isset($_POST['upgrade'])){
			update_utilisateurs($_POST["menumembre"]);
			ob_end_clean();
			header("Location: membrecontrol.php");
		}
		if(isset($_POST['ungrade'])){
			
			downgrade_utilisateurs($_POST["menumembre"]);
			
			
			$result2=sel_login($_POST["menumembre"]);
			foreach ($result2 as $x){
				if($x['login']==$_SESSION['login']){
					session_destroy();
				}
			}
			ob_end_clean();
			header("Location: membrecontrol.php");
		}
	}
	echo "<br>";
	echo "<br>";

?>
</section>	
</body>
 <?php
 }
catch(PDOException $e){
	$z=$e->getMessage();

	echo "<br>";
} ?>

<?php include("../Vue/footer.php"); ?>
</html>
