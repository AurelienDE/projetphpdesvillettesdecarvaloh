<?php
	/*
		*crypt_mdp()
		*Paramétre : Le mot de passe a crypté.
		*But : Sécuriser le mot de passe de chaque personne en le cryptant.
	*/
	function crypt_mdp ($mdp_a_crypte) {
		$mdp = $mdp_a_crypte;
		for ($i=0;$i<65535;$i++) {
			 $mdp = sha1($mdp);
			 $mdp = md5($mdp);
		 }
		 return $mdp;
	 }
	/*
		*get_motdepasse()
		*Paramétre : Le login de la personne.
		*But : Vérifier que le mot de passe de la personne
		soit bien celui qui a été rentrer pour la connexion.
	*/
	function get_motdepasse($Lelogin){		
	
	global $file_db;
			
	$result="Select motdepasse FROM utilisateur where login=:l";
	
	$result=$file_db->prepare($result);

	$le_login = $Lelogin;

	$result->bindParam(":l", $le_login);
	
    $result->execute();

    
    return $result;	
    	
}
	/*
		*get_grade()
		*Paramétre : Le login de la personne.
		*But : Connaitre le grade de la personne pour savoir les restrictions à appliquer.
	*/
	function get_grade($Lelogin){		
	
	global $file_db;
			
	$result="Select grade FROM utilisateur where login=:l";
	
	
	$result=$file_db->prepare($result);

	$le_login = $Lelogin;

	$result->bindParam(":l", $le_login);
	
    $result->execute();
    
    
    return $result;	
    	
}


	 
	 
	 	
?>
