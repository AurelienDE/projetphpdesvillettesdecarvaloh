

<?php 
/*
	*get_videoenfonctionde()
	*Paramétre : Le nom du film ou le réalisateur du film ou l'année.
	*But : Rechercher un film.
*/
function get_videosenfonctionde($tmp){		
	
	global $file_db;		
	$result0="SELECT * FROM video where nom=:n or realisateur='$tmp' or annee='$tmp'";
	
	$result0=$file_db->prepare($result0);

	 $la_vid = $tmp;
	 
	 	 
	 $result0->bindParam(":n", $la_vid);
	
    $result0->execute();
	
    return $result0;		
}
				
/*
	*get_video()
	*Paramétre : Aucun
	*But : Récupérer les informations de tous les films.
*/	
function get_video(){
	
	
	global $file_db;	
	$result="SELECT * FROM video";
			
	$result=$file_db->prepare($result);
    
    $result->execute();
	 
    return $result;		
}
			
?>
