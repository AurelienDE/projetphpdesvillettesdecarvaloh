<?php 
/*
	get_utilisateurs()
	*Paramétre : Aucun
	*But : Récuperer toutes les informations de tous les utilisateurs.
*/
function get_utilisateurs(){		
	
	global $file_db;		
	$utilisateurs="Select * FROM utilisateur";
	$utilisateurs=$file_db->prepare($utilisateurs);
    $utilisateurs ->execute();
    return $utilisateurs;		
	

   	
}
/*
	*del_utilisateurs()
	*Paramétre : Le prénom de l'utilisateur.
	*But : Supprimer un utilisateur par son prénom.
*/
function del_utilisateurs($tmp){		
	
	global $file_db;		
	$utilisateurs= "DELETE FROM utilisateur where prenom=:p ";
	$utilisateurs=$file_db->prepare($utilisateurs);
	
	$le_prenom = $tmp;	 
	$utilisateurs->bindParam(":p", $le_prenom);
	
    $utilisateurs ->execute();
    return $utilisateurs;		
	

   	
}
/*
	update_utilisateurs()
	*Paramétre : Le prénom de l'utilisateur.
	*But : Passez admin un inscrit.
*/
function update_utilisateurs($tmp){		
	
	global $file_db;		
	$utilisateurs= "UPDATE utilisateur set grade=\"admin\" where prenom=:p";
	$utilisateurs=$file_db->prepare($utilisateurs);
	
	$le_prenom = $tmp;	 
	$utilisateurs->bindParam(":p", $le_prenom);
	
    $utilisateurs ->execute();
    return $utilisateurs;		
   	
}
/*
	downgrade_utilisateurs()
	*Paramétre : Le prénom de l'utilisateur.
	*But : Passez inscrit un admin.
*/
function downgrade_utilisateurs($tmp){		
	
	global $file_db;		
	$utilisateurs= "UPDATE utilisateur set grade=\"inscrit\" where prenom=:p";
	$utilisateurs=$file_db->prepare($utilisateurs);
	
	$le_prenom = $tmp;	 
	$utilisateurs->bindParam(":p", $le_prenom);
	
    $utilisateurs ->execute();
    return $utilisateurs;		
   	
}
/*
	*sel_login()
	*Paramétre : Le prénom de l'utilisateur.
	*But : Récuperer le login avec le prénom de l'utilisateur.
*/
function sel_login($tmp){		
	
	global $file_db;		
	$utilisateurs="SELECT login FROM utilisateur where prenom=:p";
	$utilisateurs=$file_db->prepare($utilisateurs);
	
	$le_prenom = $tmp;	 
	$utilisateurs->bindParam(":p", $le_prenom);
	
    $utilisateurs ->execute();
    return $utilisateurs;		
   	
}






?>
