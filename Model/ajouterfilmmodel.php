<?php
/*
	*get_videosenfonctiondunom() 
	*Paramétre : Le nom de film
	*But : Rechercher toutes les informations du film.
*/
function get_videosenfonctiondunom($tmp){

		global $file_db;		
		
		$result="SELECT * FROM video where nom=:n";
    
		$result=$file_db->prepare($result);

		$la_vid = $tmp;	 
		$result->bindParam(":n", $la_vid);
	
		$result->execute();
	
    return $result;		
    	
}
/*
	*set_videos()
	*Paramétre : Toutes les informations du film SAUF son nom.
	*But : Modifier un fim à partir de son nom.
*/
function set_videos($real,$image,$anne,$resume,$genre,$acteur,$presentation,$nom){

		global $file_db;		
		
		$update="UPDATE video set realisateur=:r , image=:i, annee=:a,  resume=:re, genre=:g,  acteur=:ac, presentation=:p WHERE nom=:n";
		$update=$file_db->prepare($update);
		
		$le_real = $real;	 
		$la_image = $image;	
		$la_anne = $anne;	
		$le_resume = $resume;	
		$le_genre = $genre;	
		$le_acteur = $acteur;	
		$la_presentation = $presentation;	
		$le_nom = $nom;	
		

		$update->bindParam(":r", $le_real);
		$update->bindParam(":i", $la_image);
		$update->bindParam(":a", $la_anne);
		$update->bindParam(":re", $le_resume);
		$update->bindParam(":g", $le_genre);
		$update->bindParam(":ac", $le_acteur);
		$update->bindParam(":p", $la_presentation);
		$update->bindParam(":n", $le_nom);
		
		$update ->execute();
		
}
/*
	*new_videos()
	*Paramétre : Toutes les informations nécessaires à un film.
	*But : Ajouter un film à la base de donnée.
*/
function new_videos($nom,$real,$image,$annee,$resume,$genre,$acteur,$presentation){

		global $file_db;		
		
		$inserto= "INSERT INTO video (nom,realisateur,image,annee,acteur,resume,genre,presentation) VALUES (:n,:r,:i,:a,:ac,:re,:g,:p)";
		$inserto=$file_db->prepare($inserto);
		
		$le_nom = $nom;	
		$le_real = $real;	 
		$la_image = $image;	
		$la_anne = $annee;	
		$le_resume = $resume;	
		$le_genre = $genre;	
		$le_acteur = $acteur;	
		$la_presentation = $presentation;	
	
		
		$inserto->bindParam(":n", $le_nom);
		$inserto->bindParam(":r", $le_real);
		$inserto->bindParam(":i", $la_image);
		$inserto->bindParam(":a", $la_anne);
		$inserto->bindParam(":re", $le_resume);
		$inserto->bindParam(":g", $le_genre);
		$inserto->bindParam(":ac", $le_acteur);
		$inserto->bindParam(":p", $la_presentation);
	
		$inserto->execute();
		
}

/*
	*del_videos()
	*Paramétre : Le nom et l'année d'un film.
	*But : Supprimer un film à partir de son nom et de l'année.
*/
function del_videos($nom,$annee){

		global $file_db;		
		
		$del= "DELETE from video WHERE nom=:n and  annee=:a   ; )";
		$del=$file_db->prepare($del);
		
		$le_nom = $nom;	
		$la_anne = $annee;	

		$del->bindParam(":n", $le_nom);		
		$del->bindParam(":a", $la_anne);
		
		
		$del->execute();
		
}




?>

