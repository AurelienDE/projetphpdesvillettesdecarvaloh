
<?php


try{
	$file_db = new PDO("sqlite:contacts.db");
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	
 	$file_db->exec("DROP TABLE video");
	
	$file_db->exec("CREATE TABLE IF NOT EXISTS video (
					nom TEXT PRIMARY KEY,
					realisateur TEXT,
					image TEXT,
					annee TEXT,
					poucerouge INT,
					poucevert INT,
					acteur TEXT,
					resume TEXT,
					genre TEXT,
					commentaire TEXT,
					presentation TEXT)");
					
	
	$video= array(
					array("nom"=> "Gladiator","realisateur" =>"Ridley Scott", "image"=>"http://images.fan-de-cinema.com/affiches/large/ae/24157.jpg", "annee"=>"2000","poucerouge"=>0,"poucevert"=>0, "acteur"=>"Russell Crowe, Joaquin Phoenix, Connie Nielsen, Oliver Reed","resume"=>" Maximus échappe à ses assassins mais ne peut empêcher le massacre de sa famille","genre"=>"Action, Drame","commentaire"=>"","presentation"=>"Le général romain Maximus est le plus fidèle soutien de l'empereur Marc Aurèle, qu'il a conduit de victoire en victoire avec une bravoure et un dévouement exemplaires. Jaloux du prestige de Maximus, et plus encore de l'amour que lui voue l'empereur, le fils de Marc-Aurèle, Commode, s'arroge brutalement le pouvoir, puis ordonne l'arrestation du général et son execution. Maximus échappe à ses assassins mais ne peut empêcher le massacre de sa famille. Capturé par un marchand d'esclaves, il devient gladiateur et prépare sa vengeance." ),
					array("nom"=> "Will Hunting","realisateur" =>" Gus Van Sant", "image"=>"http://img.filmsactu.net/datas/films/w/i/will-hunting/xl/46f876f5dfe00.jpg", "annee"=>"1998","poucerouge"=>0,"poucevert"=>0,"acteur"=>"Robin Williams, Matt Damon, Ben Affleck, Stellan Skarsgård","resume"=>"Un délinquant surdoué rencontre un psychothérapeute ","genre"=>"Drame ","commentaire"=>"", "presentation"=>"Dans la banlieue pauvre de Boston, Will Hunting erre de job en job. Il passe sa vie dans les bars avec ses copains Chukie, Morgan et Billy, à chahuter, boire, draguer ou chercher la bagarre. Mais grâce à son talent prodigieux pour les mathématiques, Will attire l'attention du professeur Lambeau. Il accepte, non sans réticence, sa protection afin d'éviter la prison. Lambeau le présente alors au psychothérapeute Sean Mc Guire...
" ),
					array("nom"=> "300","realisateur" =>"Zack Snyder", "image"=>"http://az-movies.a.z.pic.centerblog.net/o/a80de452.jpg", "annee"=>"2007","poucerouge"=>0,"poucevert"=>0,"acteur"=>"Gerard Butler, Lena Headey, David Wenham, Dominic West","resume"=>"300 grecs s'opposant à des millers de perses","genre"=>"Guerre, Historique","commentaire"=>"", "presentation"=>"Adapté du roman graphique de Frank Miller, 300 est un récit épique de la Bataille des Thermopyles, qui opposa en l'an - 480 le roi Léonidas et 300 soldats spartiates à Xerxès et l'immense armée perse. Face à un invincible ennemi, les 300 déployèrent jusqu'à leur dernier souffle un courage surhumain ; leur vaillance et leur héroïque sacrifice inspirèrent toute la Grèce à se dresser contre la Perse, posant ainsi les premières pierres de la démocratie. " ),
					array("nom"=> "Fight Club","realisateur" =>"David Fincher", "image"=>"http://vultureculture.fr/blog/wp-content/uploads/2011/03/02-01-11_062147_1188332213_fight-club-dvd1.jpg", "annee"=>"1999","poucerouge"=>0,"poucevert"=>0,"acteur"=>"Edward Norton, Brad Pitt, Helena Bonham Carter, Meat Loaf","resume"=>"Thriller d'un homme qui veut changer de mode de vie","genre"=>"Drame, Thrille","commentaire"=>"", "presentation"=>"Très vite, il rencontre Tyler Durden, un vendeur de savon charismatique à la philosophie tordue et anarchiste. Ensemble, ils décident de créer un club de combat clandestin dont les règles s'établissent partout dans le monde. Règle n° 1 : Il est interdit de parler du Fight Club. Règle n° 2 : Il est INTERDIT de parler du Fight Club. " ),
					
					array("nom"=> "V pour Vendetta","realisateur" =>"James McTeigue", "image"=>"http://images.fan-de-cinema.com/affiches/science_fiction/v_pour_vendetta,7.jpg", "annee"=>"2006","poucerouge"=>0,"poucevert"=>0,"acteur"=>"Natalie Portman, Hugo Weaving, Stephen Rea, Stephen Fry","resume"=>"Une histoire passionante qui suit la révolution d'une société","genre"=>"Drame, Fantastique","commentaire"=>"", "presentation"=>"Londres, au 21ème siècle...
Evey Hammond ne veut rien oublier de l'homme qui lui sauva la vie et lui permit de dominer ses peurs les plus lointaines. Mais il fut un temps où elle n'aspirait qu'à l'anonymat pour échapper à une police secrète omnipotente. Comme tous ses concitoyens, trop vite soumis, elle acceptait que son pays ait perdu son âme et se soit donné en masse au tyran Sutler et à ses partisans.
Une nuit, alors que deux gardiens de l'ordre s'apprêtaient à la violer dans une rue déserte, Evey vit surgir son libérateur. Et rien ne fut plus comme avant.
Son apprentissage commença quelques semaines plus tard sous la tutelle de V. Evey ne connaîtrait jamais son nom et son passé, ne verrait jamais son visage atrocement brûlé et défiguré, mais elle deviendrait à la fois son unique disciple, sa seule amie et le seul amour d'une vie sans amour..." ),
					);
	
	$insert= "INSERT INTO video (nom,realisateur,image,annee,poucerouge,poucevert,acteur,resume,genre,commentaire,presentation) VALUES (:n, :p, :e, :l, :pr, :pv, :a,:r,:g,:c,:m)";
	$stmt=$file_db->prepare($insert);
	
	$le_nom = null;
	$le_realisateur=null;
	$les_images=null;
	$les_annes=null;
	$poucerouge=null;
	$poucevert=null;
	$les_acteurs=null;
	$les_resume=null;
	$les_genres=null;
	$commentaire=null;
	$la_presentation=null;
	
	$stmt->bindParam(":n", $le_nom);
	$stmt->bindParam(":p", $le_realisateur);
	$stmt->bindParam(":pr", $poucerouge);
	$stmt->bindParam(":pv", $poucevert);
	$stmt->bindParam(":e", $les_images);
	$stmt->bindParam(":l", $les_annes);
	$stmt->bindParam(":a", $les_acteurs);
	$stmt->bindParam(":r", $les_resume);
	$stmt->bindParam(":g", $les_genres);
	$stmt->bindParam(":c", $commentaire);
	$stmt->bindParam(":m", $la_presentation);
	
	foreach ($video as $c){
		$le_nom = $c["nom"];
		$le_realisateur=$c["realisateur"];
		$les_images=$c["image"];
		$les_annes=$c["annee"];
		$poucerouge=$c["poucerouge"];
		$poucevert=$c["poucevert"];
		$les_acteurs=$c["acteur"];
		$les_resume=$c["resume"];
		$les_genres=$c["genre"];
		$commentaire=$c["commentaire"];
	
		$la_presentation=$c["presentation"];
				
		$stmt ->execute();
	}
	 
	echo "contacts en base";
	
	 

}

catch(PDOException $e){
	echo $e->getMessage();
}

?>
