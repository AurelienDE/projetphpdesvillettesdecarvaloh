<?php 
/*
	*get_videoenfonctionde()
	*Paramétre : Le titre du film.
	*But : Connaitre toutes les informations d'un film à partir de son nom.
*/
function get_videosenfonctionde($tmp){		
	
		global $file_db;		
		
		$result="SELECT * FROM video where nom=:n";
    
		$result=$file_db->prepare($result);

		$la_vid = $tmp;	 
		$result->bindParam(":n", $la_vid);
	
		$result->execute();
	
    return $result;	
}
/*
	*get_pourcerouge_poucevert()
	*Paramétre : Le nom du film.
	*But : Récuperer le nombre de pouce vert et le nombre de
	pouce rouge qu'un film posséde.
*/
function get_poucerouge_poucevert($tmp){		
	
	global $file_db;		
	$result="SELECT poucerouge, poucevert FROM video where nom=:n";
	$result=$file_db->prepare($result);

	$le_nom = $tmp;	 
	$result->bindParam(":n", $le_nom);
	
	$result->execute();
	

    
    return $result;		
}
/*
	*set_pourcerouge()
	*Paramétre : Le nom du film.
	*But : Ajouter un pouce rouge au film.
*/
function set_poucerouge($tmp){		
	
	global $file_db;
	$update="UPDATE video set poucerouge=poucerouge+1 WHERE nom=:n";
	$update=$file_db->prepare($update);
	
	$le_nom = $tmp;	 
	$update->bindParam(":n", $le_nom);
	
	
	$update ->execute();	
}
/*
	*set_poucevert()
	*Paramétre : Le nom de film.
	*But : Ajouter un pouce vert au film.
*/
function set_poucevert($tmp){		
	
	global $file_db;
	$update="UPDATE video set poucevert=poucevert+1 WHERE nom=:n";
	$update=$file_db->prepare($update);
	
	$le_nom = $tmp;	 
	$update->bindParam(":n", $le_nom);
	
	$update ->execute();	
}
/*
	*set_commentaire()
	*Paramétre : Le nom du film et tout les commentaires.
	*But : Ajouter un commentaire au film. (tout les commentaire + le nouveau commentaire)
*/
function set_commentaire($tmp,$tmpp){		
	
	global $file_db;
	$update= "UPDATE video set commentaire=:c WHERE nom=:n ";
	$update=$file_db->prepare($update);
	
	$le_com = $tmp;	
	$le_nom = $tmpp;	 
	
	$update->bindParam(":c", $le_com);
	$update->bindParam(":n", $le_nom);
	
	$update ->execute();
}
/*
	*set_nombremessage()
	*Paramétre : Le login de la personne.
	*But : Incrémenter de 1 le nombre de message posté par la personne.
*/
function set_nombremessage($tmp){		
	
	global $file_db;
	$update= "UPDATE utilisateur set nombremessage=nombremessage+1 WHERE login=:l";
	$update=$file_db->prepare($update);
	
	
	$le_login = $tmp;	
	
	$update->bindParam(":l", $le_login);
	
	$update ->execute();
}
/*
	*del_nombremessage()
	*Paramétre : Le login de la personne.
	*But : Décrémenter de 1 le nombre de message posté par la personne.
*/
function del_nombremessage($tmp){		
	
	global $file_db;
	$update= "UPDATE utilisateur set nombremessage=nombremessage-1 WHERE login=:l";
	$update=$file_db->prepare($update);
	
	$le_login = $tmp;	
	
	$update->bindParam(":l", $le_login);
	
	$update ->execute();
}
/*
	*remise_a_zero()
	*Paramétre : Le titre du film.
	*But : Pour les administrateurs permettent de remettre à 0 le nombre de vote.
*/
function remise_a_zero($tmp){		
	
	global $file_db;
	$update="UPDATE video set poucerouge=0, poucevert=0  WHERE nom=:n";
	$update=$file_db->prepare($update);
	
	$le_nom = $tmp;	
	
	$update->bindParam(":n", $le_nom);
	
	$update ->execute();
}




?>


