<?php
/*
	*get_commentaire()
	*Paramétre : Aucun
	*But : Récupérer tout les commentaires de chaque film.
*/
function get_commentaire(){		
	
	global $file_db;		
	$lescom="SELECT commentaire FROM video ";
	$lescom=$file_db->prepare($lescom);
    $lescom ->execute();
    return $lescom;		
}
/*
	*get_commentaireetnom()
	*Paramétre : Aucun
	*But : Récupérer tout les commentaires et les nom de chaque film.
*/
function get_commentaireetnom(){		
	
	global $file_db;		
	$lescometnom="SELECT nom,commentaire FROM video ";
	$lescometnom=$file_db->prepare($lescometnom);
    $lescometnom ->execute();
    return $lescometnom;			
}
/*
	*get_videoenfonctiondunom()
	*Paramétre : Le nom du film
	*But : Récupérer toutes les informations du film.
*/

function get_videoenfonctiondunnom($unnom){		
	
	global $file_db;		
	$result="SELECT * FROM video where nom=:n";
	$result=$file_db->prepare($result);
	
	$le_nom = $unnom;	
	$result->bindParam(":n", $le_nom);
    
    $result ->execute();
    return $result;		
}

?>
