<!doctype html>
<html>
<link rel="stylesheet" type="text/css" href="design.css"/>
<body>
<?php
ini_set('display_errors', 1);
error_reporting(E_ALL); 
date_default_timezone_set('Europe/Paris');
try{
	// Création de la base SQLite
	$file_db=new PDO('sqlite:contacts.db');
	// Géré le niveau des erreurs rapportés
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	//LISTE A PUCE 
	//$result=$file_db->query('SELECT * FROM contacts');
	//foreach($result as $m){
	//	echo "<ul><br>\n<li>".$m['prenom'].'</li><li> '.$m['nom'].'</li><li> '.date('Y-m-d H:i:s',$m['time']).'</li>';
	//}
	if(isset($_GET["cb"])){
			foreach($_GET["cb"] as $id){
				$delete=$file_db->prepare("DELETE FROM contacts where id=:id");
				$delete->execute(array('id' => $id));	
			}
		}
	$result=$file_db->query('SELECT * FROM contacts');
	if (isset($_GET["prenom"])){
		$text=$_GET["prenom"];
		if (preg_match("/prenom=([a-z]+)/",$text)){
			echo "<tr><th>Prenom</th><td>".$m['prenom']."</td></tr>";
		}
	}
	
	$compteur=0;
	if(((empty($_GET["prenom"])) and (empty($_GET["nom"])) and (empty($_GET["time"])))){
		echo"<form method=\"get\" action=\"listetable.php\"><table border=3>";
		echo"<tr><th>Prenom</th><th>Nom</th><th>Time</th></tr>";
		foreach($result as $m){
			++$compteur;	
			if($compteur%2==1){
				echo "<tr class='casetableau'><td>".$m['prenom'].'</td><td> '.$m['nom'].'</td><td> '.date('Y-m-d',$m['time']).'</td><td><input type="checkbox" name="cb[]" value="'.$m["id"].'"></td></tr>';
			}
			else{
				echo "<tr><td>".$m['prenom'].'</td><td> '.$m['nom'].'</td><td> '.date('Y-m-d',$m['time']).'</td><td><input type="checkbox" name="cb[]" value="'.$m["id"].'"></td></tr>';
			}
		}
		echo"</table><input type=\"submit\" name=\"supprimer\" value=\"supprimer\"/></form>";
	}
	else{
		if(!empty($_GET["Recherche"])){
			if(!empty($_GET["prenom"]) and empty($_GET["nom"])){
				$tmp=$_GET['prenom'];
				$result=$file_db->query("SELECT * FROM contacts where prenom='$tmp'");
				//$result2="SELECT * FROM contacts where prenom=:p";
				echo"<form method=\"get\" action=\"listetable.php\"><table border=3>";
				echo"<tr><th>Prenom</th><th>Nom</th><th>Time</th></tr>";
				$compteur=0;
				foreach ($result as $p){
					++$compteur;
					if($compteur%2==1){
						echo "<tr class='casetableau'><td>".$p['prenom'].'</td><td> '.$p['nom'].'</td><td> '.date('Y-m-d',$p['time']).'</td><td><input type="checkbox" name="cb[]" value="'.$p["id"].'"></td></tr>';
					}
					else{
						echo "<tr><td>".$p['prenom'].'</td><td> '.$p['nom'].'</td><td> '.date('Y-m-d',$p['time']).'</td><td><input type="checkbox" name="cb[]" value="'.$p["id"].'"></td></tr>';
					}
				}
				echo"</table><input type=\"submit\" name=\"supprimer\" value=\"supprimer\"/></form>";
			}
			elseif(!empty($_GET["nom"]) and empty($_GET["prenom"])){
				$tmp=$_GET['nom'];
				$result=$file_db->query("SELECT * FROM contacts where nom='$tmp'");
				//$result2="SELECT * FROM contacts where prenom=:p";
				echo"<form method=\"get\" action=\"listetable.php\"><table border=3>";
				echo"<tr><th>Prenom</th><th>Nom</th><th>Time</th></tr>";
				$compteur=0;
				foreach ($result as $p){
					++$compteur;
					if($compteur%2==1){
						echo "<tr class='casetableau'><td>".$p['prenom'].'</td><td> '.$p['nom'].'</td><td> '.date('Y-m-d',$p['time']).'</td><td><input type="checkbox" name="cb[]" value="'.$p["id"].'"></td></tr>';
					}
					else{
						echo "<tr><td>".$p['prenom'].'</td><td> '.$p['nom'].'</td><td> '.date('Y-m-d',$p['time']).'</td><td><input type="checkbox" name="cb[]" value="'.$p["id"].'"></td></tr>';
					}
				}
				echo"</table><input type=\"submit\" name=\"supprimer\" value=\"supprimer\"/></form>";
			}
			elseif(!empty($_GET["nom"]) and (!empty($_GET["prenom"]))){
				$tmp=$_GET['nom'];
				$tmp2=$_GET['prenom'];
				$result=$file_db->query("SELECT * FROM contacts where nom='$tmp' and prenom='$tmp2'");
				//$result2="SELECT * FROM contacts where prenom=:p";
				echo"<form method=\"get\" action=\"listetable.php\"><table border=3>";
				echo"<tr><th>Prenom</th><th>Nom</th><th>Time</th></tr>";
				$compteur=0;
				foreach ($result as $p){
					++$compteur;
					if($compteur%2==1){
						echo "<tr class='casetableau'><td>".$p['prenom'].'</td><td> '.$p['nom'].'</td><td> '.date('Y-m-d',$p['time']).'</td><td><input type="checkbox" name="cb[]" value="'.$p["id"].'"></td></tr>';
					}
					else{
						echo "<tr><td>".$p['prenom'].'</td><td> '.$p['nom'].'</td><td> '.date('Y-m-d',$p['time']).'</td><td><input type="checkbox" name="cb[]" value="'.$p["id"].'"></td></tr>';
					}
				}
				echo"</table><input type=\"submit\" name=\"supprimer\" value=\"supprimer\"/></form>";
			}
			else{
				echo "Time n'est pas un parametre de recherche.";
			}
		}
		else{
			$tmp=$_GET['nom'];
			$tmp2=$_GET['prenom'];
			$tmp3=strtotime($_GET['time']);
			$insert=$file_db->prepare("INSERT INTO contacts(nom,prenom,time) VALUES (:nom, :prenom, :date)");
			$insert->execute(array('nom' => $tmp, 'prenom' => $tmp2, 'date' => $tmp3));
		}
	}

	$file_db=null;
}
catch(PDOException $e){
	echo $e->getMessage();
}
?>

<form method="GET" action="listetable.php">
	<em> Prenom :</em>
		<input type="prenom" name="prenom">
	<br>
	<em> Nom :</em>
		<input type="nom" name="nom">
	<br>
	<em> Time : </em>
		<input type="time" name="time">
	<br>
		<input name="Recherche" type="submit" value="Rechercher">
		<input name="Ajout" type="submit" value="Ajouter">
</form>
<br> Indication : Pour afficher toute la table cliquer sur rechercher sans aucun parametre.
</html>