<?php
date_default_timezone_set('Europe/Paris');
try{
	// Création de la base SQLite
	$file_db=new PDO('sqlite:contacts.db');
	// Géré le niveau des erreurs rapportés
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	//Pour creer la base uniquement en mémoie
	//$memory_db=new PDO('sqlite::memory:');

	$file_db->exec("CREATE TABLE IF NOT EXISTS contacts(
		id INTEGER PRIMARY KEY,
		nom TEXT,
		prenom TEXT,
		time INTEGER)");
	$contacts=array(
		array( 'id'=> 1,
			'nom'=> 'De Carvalho',
			'prenom'=> 'William',
			'time'=> strtotime("1945-08-23")),
		array('id'=> 2,
			'nom'=> 'Talon',
			'prenom'=> 'Achille',
			'time'=> strtotime("1955-08-23")),
		array('id'=> 3,
			'nom'=> 'Higgs',
			'prenom'=> 'Peter',
			'time'=> strtotime("1955-08-23"))
		);
	$insert= "INSERT INTO contacts(nom,prenom,time) VALUES (:n,:p,:t)";
	$stmt=$file_db->prepare($insert);

	$le_nom=null;
	$le_prenom=null;
	$le_time=null;

	$stmt->bindParam(":n",$le_nom);
	$stmt->bindParam(":p",$le_prenom);
	$stmt->bindParam(":t",$le_time);

	foreach($contacts as $c){
		$le_nom=$c["nom"];
		$le_prenom=$c["prenom"];
		$le_time=$c["time"];

		$stmt->execute();
	}
	echo "contacts en base !";



	$file_db=null;
}
catch(PDOException $e){
	echo $e->getMessage();
}